var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var bodyParser = require("body-parser");
var fs = require("fs");

var indexRouter = require("./routes/index");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({ extended: false })); // Middleware
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);

/* Recuperer les données du form et les ecrire sur un fichier JSON */
app.post("/inscription", function(req, res) {
  var register = req.body; // variable qui content toute les données du formulaires

  saveRegisterToPublicFolder(register, function(err) {
    // msg d'erreur en cas d'echec de la fonction
    if (err) {
      res.status(404).send("Utilisateur non enregistré");
      return;
    } else {
      res.send("Utilisateur enregistré");
    }
  });
});

function saveRegisterToPublicFolder(register, callback) {
  fs.appendFile(
    "./public/inscription.json",
    JSON.stringify(register),
    callback
  ); // Converti et écrit les données JS en Json
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
