// nav hamburger

var mainNav = document.getElementById("js-menu");
var navBarToggle = document.getElementById("js-navbar-toggle");

navBarToggle.addEventListener("click", function() {
  mainNav.classList.toggle("active");
});

// horloge
var clock = document.getElementById("clock");
var hexColor = document.getElementById("hex-color");

function hexClock() {
  var time = new Date();
  var hours = time.getHours().toString();
  var minutes = time.getMinutes().toString();
  var seconds = time.getSeconds().toString();

  var hoursRandom = Math.ceil(Math.random() * 9);
  var minutesRandom = Math.ceil(Math.random() * 9);
  var secondsRandom = Math.ceil(Math.random() * 9);

  if (hours.length < 2) {
    hours = "0" + hours; // en cas de mono digit
  }

  if (minutes.length < 2) {
    minutes = "0" + minutes;
  }

  if (seconds.length < 2) {
    seconds = "0" + seconds;
  }

  var clockStr = hours + " : " + minutes + " : " + seconds; // affichage de l'horloge
  var hexColorStr = "#" + hoursRandom + minutesRandom + secondsRandom; // affichage du code hex par rapport a l'heure

  clock.textContent = clockStr; // applique la variable d'affichage

  if (time.getSeconds() % 2) {
    hexColor.textContent = hexColorStr;
    document.getElementById("clock").style.color = hexColorStr; // changement de la couleur de l'horloge avec le code hex chaque seconde
  }
}

hexClock();
setInterval(hexClock, 1000); // 1sec

/* ACTIVE LINK */

// Get the container element
var btnContainer = document.getElementById("js-menu");

// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("nav-links");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}

/* VALIDATION FORMULAIRE */
function checkForm() {
  // champs
  const nom = document.getElementById("last-name").value;
  const prenom = document.getElementById("first-name").value;
  const birthDate = document.getElementById("birthdate").value;
  const adresse = document.getElementById("address").value;
  const zipCode = document.getElementById("zip").value;
  const ville = document.getElementById("city").value;
  const mail = document.getElementById("mail").value;
  const tel = document.getElementById("phone").value;
  //regex
  var regName = /^[a-z ,.'-]+$/i;
  var regZipCode = /^\d{5}$|^\d{5}-\d{4}$/;

  // Champ nom
  if (nom.trim() == "") {
    // verifie si champ vide trim prend en compte les espaces
    document.getElementById("msg-form-1").classList.add("danger");
    document.getElementById("msg-form-1").innerHTML = "Entrez votre nom";
    return false;
  } else if (!regName.test(nom)) {
    // controle la validité de la syntaxe
    document.getElementById("msg-form-1").classList.add("danger");
    document.getElementById("msg-form-1").innerHTML = "Entrez un nom valide";
    return false;
  }

  // Champ prenom
  if (prenom.trim() == "") {
    // verifie si champ vide trim(). prend en compte les espaces
    document.getElementById("msg-form-2").classList.add("danger");
    document.getElementById("msg-form-2").innerHTML = "Entrez votre prenom";
    return false;
  } else if (!regName.test(prenom)) {
    // controle la validité de la syntaxe
    document.getElementById("msg-form-2").classList.add("danger");
    document.getElementById("msg-form-2").innerHTML = "Entrez un prenom valide";
    return false;
  }

  // champ birthDate
  if (birthDate == "") {
    document.getElementById("msg-form-3").classList.add("danger");
    document.getElementById("msg-form-3").innerHTML =
      "Entrez votre date de naissance";
    return false;
  }

  // champ adresse
  if (adresse.trim() == "") {
    document.getElementById("msg-form-4").classList.add("danger");
    document.getElementById("msg-form-4").innerHTML = "Entrez votre adresse";
    return false;
  } else if (!regName.test(adresse)) {
    // controle la validité de la syntaxe
    document.getElementById("msg-form-4").classList.add("danger");
    document.getElementById("msg-form-4").innerHTML =
      "Entrez une adresse valide";
    return false;
  }

  // champ code postal
  if (zipCode.trim() == "") {
    document.getElementById("msg-form-5").classList.add("danger");
    document.getElementById("msg-form-5").innerHTML =
      "Entrez votre Code postal";
    return false;
  } else if (!regZipCode.test(zipCode)) {
    // controle la validité de la syntaxe
    document.getElementById("msg-form-5").classList.add("danger");
    document.getElementById("msg-form-5").innerHTML =
      "Entrez un code postal valide";
    return false;
  }

  if (ville.trim() == "") {
    document.getElementById("msg-form-6").classList.add("danger");
    document.getElementById("msg-form-6").innerHTML = "Entrez votre ville";
    return false;
  } else if (!regName.test(ville)) {
    // controle la validité de la syntaxe
    document.getElementById("msg-form-6").classList.add("danger");
    document.getElementById("msg-form-6").innerHTML = "Entrez une ville valide";
    return false;
  }

  if (mail.trim() == "") {
    document.getElementById("msg-form-7").classList.add("danger");
    document.getElementById("msg-form-7").innerHTML = "Entrez votre email";
    return false;
  }

  if (tel.trim() == "") {
    document.getElementById("msg-form-8").classList.add("danger");
    document.getElementById("msg-form-8").innerHTML =
      "Entrez votre numéro de tel";
    return false;
  }

  return true;
}

/* SLIDEr */
var slideIndex = 1;

var myTimer;

window.addEventListener("load", function() {
  showSlides(slideIndex);
  myTimer = setInterval(function() {
    plusSlides(1);
  }, 4000);
});

// NEXT AND PREVIOUS CONTROL
function plusSlides(n) {
  clearInterval(myTimer);
  if (n < 0) {
    showSlides((slideIndex -= 1));
  } else {
    showSlides((slideIndex += 1));
  }
  if (n === -1) {
    myTimer = setInterval(function() {
      plusSlides(n + 2);
    }, 4000);
  } else {
    myTimer = setInterval(function() {
      plusSlides(n + 1);
    }, 4000);
  }
}

//Controls the current slide and resets interval if needed
function currentSlide(n) {
  clearInterval(myTimer);
  myTimer = setInterval(function() {
    plusSlides(n + 1);
  }, 4000);
  showSlides((slideIndex = n));
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {
    slideIndex = 1;
  }
  if (n < 1) {
    slideIndex = slides.length;
  }
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex - 1].style.display = "block";
}
