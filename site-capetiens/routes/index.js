var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function(req, res) {
  res.render("index", { page: "Accueil" });
});

router.get("/inscription", function(req, res) {
  res.render("inscription", { page: "inscription" });
});

router.get("/hugues-capet", function(req, res) {
  res.render("hugues-capet", { page: "Hugues-capet" });
});

router.get("/louisIX", function(req, res) {
  res.render("louisIX", { page: "louisIX" });
});

router.get("/philippe-le-bel", function(req, res) {
  res.render("philippe-le-bel", {
    page: "philippe-le-bel"
  });
});

router.get("/philippe-auguste", function(req, res) {
  res.render("philippe-auguste", {
    page: "philippe-auguste"
  });
});

module.exports = router;
